package com.appium.example.constructors;

import com.appium.example.BaseTest;
import com.appium.example.pages.bookmark.*;
import com.appium.example.pages.homepage.HomeAndroid;
import com.appium.example.pages.homepage.HomeiOS;

//import static com.appium.example.constructors.HomeConstructor.PLATFORM;

/**
 * Created by RolandC on 2017-09-28.
 * Initialize the bookmark screen page object based on platform
 */
public class BookmarkConstructor {

    private BookmarkBase myBookmark;
//    String platform = "iOS";

    public BookmarkConstructor(String platform) throws Exception {

        if (platform.toLowerCase().equals(BaseTest.iOS_PLATFORM.toLowerCase())) {
            myBookmark = new BookmarkiOS();
        } else {
            throw new Exception(String.format("Not a valid platform to execute tests: %s", platform));
        }
    }

    public BookmarkBase getMyBookmark() { return myBookmark; }
}

//        if (platform.toLowerCase().equals(BaseTest.ANDROID_PLATFORM.toLowerCase())) {
//            myBookmark = new BookmarkAndroid();
//        } else if (platform.toLowerCase().equals(BaseTest.iOS_PLATFORM.toLowerCase())) {
//            myBookmark = new BookmarkiOS();
//        }
//        else {
//            throw new Exception(String.format("Not a valid platform to execute tests: %s", platform));
//        }



