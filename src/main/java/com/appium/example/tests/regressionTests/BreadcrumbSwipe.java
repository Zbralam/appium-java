package com.appium.example.tests.regressionTests;

import com.appium.example.Logger;
import com.appium.example.tests.TEST_Base;
import org.junit.Test;


public class BreadcrumbSwipe extends TEST_Base {

    private static final String URL_TEST_MOBILE_WEBSITE = "https://www.zoro.com/dewalt-string-trimmer-line-0080in-dia-225-ft-dwo1dt802/i/G9981745/";
    private static final String URL_TEST_WEBSITE = "";

    @Test
    public void breadCrumbSwipe() throws Exception {

        Logger.logAction("Begin: breadCrumbSwipe() - Zoro Mobile Test");

        Logger.logAction("Open Safari Browser in simulator \n" +
                "\t Browse to a mobile website \n" +
                "\t Test-5 Swipe on Breadcrumb \n" );

        Logger.logStep("Open Chrome, browse to: " + URL_TEST_MOBILE_WEBSITE);
        launchWebSite(URL_TEST_MOBILE_WEBSITE);

        Logger.logStep("Test-5 Swipe on Breadcrumb");
        swipeOnBreadcrumb();
    }
}
