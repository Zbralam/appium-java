package com.appium.example.tests.regressionTests;

import com.appium.example.Logger;
import com.appium.example.tests.TEST_Base;
import org.junit.Test;


public class ScrollUpAndDown extends TEST_Base {

    private static final String URL_TEST_MOBILE_WEBSITE = "https://www.zoro.com/werner-extension-ladder-fiberglass-24-ft-ia-d6224-2/i/G2739292/";
    private static final String URL_TEST_WEBSITE = "";

    @Test
    public void ScrollUpAndDown() throws Exception {

        Logger.logAction("Begin: ScrollUpAndDown() - Zoro Mobile Test");

        Logger.logAction("Open Safari Browser in simulator \n" +
                "\t Browse to a mobile website \n" +
                "\t Test-2 Scroll the Page to Top to Bottom and vice versa \n" );

        Logger.logStep("Open Chrome, browse to: " + URL_TEST_MOBILE_WEBSITE);
        launchWebSite(URL_TEST_MOBILE_WEBSITE);

        Logger.logStep("Test-2 Scroll the Page to Top to Bottom and vice versa");
        scrollUpAndDown();
    }
}
