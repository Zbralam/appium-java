package com.appium.example.tests.regressionTests;

import com.appium.example.Logger;
import com.appium.example.tests.TEST_Base;
import org.junit.Test;


public class ZoomInAndZoomOut extends TEST_Base {

    private static final String URL_TEST_MOBILE_WEBSITE = "https://www.zoro.com/werner-extension-ladder-fiberglass-24-ft-ia-d6224-2/i/G2739292/";
    private static final String URL_TEST_WEBSITE = "";

    @Test
    public void ZoomInAndZoomOut() throws Exception {

        Logger.logAction("Begin: ZoomInAndZoomOut() - Zoro Mobile Test");

        Logger.logAction("Open Safari Browser in simulator \n" +
                "\t Browse to a mobile website \n" +
                "\t Test-1 Zoom in and out on Picture \n" );

        Logger.logStep("Open Chrome, browse to: " + URL_TEST_MOBILE_WEBSITE);
        launchWebSite(URL_TEST_MOBILE_WEBSITE);

        Logger.logStep("Test-1 Zoom in and out on Picture");
        zoomInAndZoomOut();
    }
}
