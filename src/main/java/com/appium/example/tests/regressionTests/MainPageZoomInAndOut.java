package com.appium.example.tests.regressionTests;

import com.appium.example.Logger;
import com.appium.example.tests.TEST_Base;
import org.junit.Test;


public class MainPageZoomInAndOut extends TEST_Base {

    private static final String URL_TEST_MOBILE_WEBSITE = "https://www.zoro.com";
    private static final String URL_TEST_WEBSITE = "";

    @Test
    public void mainPageZoomFuntion() throws Exception {

        Logger.logAction("Begin: mainPageZoomFuntion() - Zoro Mobile Test");

        Logger.logAction("Open Safari Browser in simulator \n" +
                "\t Browse to a mobile website \n" +
                "\t Test-6 Zoom in and out on Main Page \n" );

        Logger.logStep("Open Chrome, browse to: " + URL_TEST_MOBILE_WEBSITE);
        launchWebSite(URL_TEST_MOBILE_WEBSITE);

        Logger.logStep("Test-6 Zoom in and out on Main Page");
        mainPageZoomInAndZoomOut();
    }
}
